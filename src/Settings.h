#pragma once

/*
*      Copyright (C) 2005-2014 Team XBMC
*      http://www.xbmc.org
*
*  This Program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2, or (at your option)
*  any later version.
*
*  This Program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with XBMC; see the file COPYING.  If not, write to
*  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
*  http://www.gnu.org/copyleft/gpl.html
*
*/

#include <string>
#include "xbmc_addon_types.h"

  /**
   * Represents the current addon settings
   */
class Settings {
  public:
    static const bool        DEFAULT_ENABLED_TV;
    static const bool        DEFAULT_ENABLED_RADIO;

    /**
     * Singleton getter for the instance
     */
    static Settings& GetInstance()
    {
      static Settings settings;
      return settings;
    }

    /**
     * Read all settings defined in settings.xml
     */
    void ReadSettings();

    /**
     * Set a value according to key definition in settings.xml
     */
    ADDON_STATUS SetSetting(const std::string &key, const void *value);

    /**
     * Getters for the settings values
     */
    bool        IsEnabledTV() const { return m_IsEnabledTV; }
    bool        IsEnabledRadio() const { return m_IsEnabledRadio; }

  private:
    Settings()
    : m_IsEnabledTV(DEFAULT_ENABLED_TV),
      m_IsEnabledRadio(DEFAULT_ENABLED_RADIO) {}

    Settings(Settings const &) = delete;
    void operator=(Settings const &) = delete;

    /**
     * Setters
     */
    void SetIsEnabledTV(bool value) { m_IsEnabledTV = value; }
    void SetIsEnabledRadio(bool value) { m_IsEnabledRadio = value; }

    /**
     * Read/Set values according to definition in settings.xml
     */
    static std::string ReadStringSetting(const std::string &key, const std::string &def);
    static int         ReadIntSetting(const std::string &key, int def);
    static bool        ReadBoolSetting(const std::string &key, bool def);

    // @return ADDON_STATUS_OK if value has not changed, ADDON_STATUS_NEED_RESTART otherwise
    static ADDON_STATUS SetStringSetting(const std::string &oldValue, const void *newValue);
    static ADDON_STATUS SetIntSetting(int oldValue, const void *newValue);
    static ADDON_STATUS SetBoolSetting(bool oldValue, const void *newValue);

    bool        m_IsEnabledTV;
    bool        m_IsEnabledRadio;
};