/*
 *      Copyright (C) 2005-2011 Team XBMC
 *      http://www.xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */
#include "client.h"
#include "Settings.h"

using namespace ADDON;

const bool        Settings::DEFAULT_ENABLED_TV        = true;
const bool        Settings::DEFAULT_ENABLED_RADIO     = true;

void Settings::ReadSettings()
{
  SetIsEnabledTV(ReadBoolSetting("tv_enabled", DEFAULT_ENABLED_TV));
  SetIsEnabledRadio(ReadBoolSetting("radio_enabled", DEFAULT_ENABLED_RADIO));
}

ADDON_STATUS Settings::SetSetting(const std::string &key, const void *value)
{
  /* Connection */
  if (key == "tv_enabled")
    return SetBoolSetting(IsEnabledTV(), value);
  else if (key == "radio_enabled")
    return SetBoolSetting(IsEnabledRadio(), value);
  else
  {
	XBMC->Log(LOG_DEBUG, "%s - unknown setting '%s'.", __FUNCTION__, key.c_str());
    return ADDON_STATUS_UNKNOWN;
  }
}

std::string Settings::ReadStringSetting(const std::string &key, const std::string &def)
{
  char value[1024];
  if (XBMC->GetSetting(key.c_str(), value))
    return value;

  return def;
}

int Settings::ReadIntSetting(const std::string &key, int def)
{
  int value;
  if (XBMC->GetSetting(key.c_str(), &value))
    return value;

  return def;
}

bool Settings::ReadBoolSetting(const std::string &key, bool def)
{
  bool value;
  if (XBMC->GetSetting(key.c_str(), &value))
    return value;

  return def;
}

ADDON_STATUS Settings::SetStringSetting(const std::string &oldValue, const void *newValue)
{
  if (oldValue == std::string(reinterpret_cast<const char *>(newValue)))
    return ADDON_STATUS_OK;

  return ADDON_STATUS_NEED_RESTART;
}

ADDON_STATUS Settings::SetIntSetting(int oldValue, const void *newValue)
{
  if (oldValue == *(reinterpret_cast<const int *>(newValue)))
    return ADDON_STATUS_OK;

  return ADDON_STATUS_NEED_RESTART;
}

ADDON_STATUS Settings::SetBoolSetting(bool oldValue, const void *newValue)
{
  if (oldValue == *(reinterpret_cast<const bool *>(newValue)))
    return ADDON_STATUS_OK;

  return ADDON_STATUS_NEED_RESTART;
}

