/*
 *      Copyright (C) 2011 Pulse-Eight
 *      http://www.pulse-eight.com/
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */
#include <stdio.h>
#include <string.h>    //strlen
#include <stdlib.h>    //strlen
#include <sys/socket.h> //socket
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <fstream>  
#include <iostream> 
#include <time.h>
#include "DVDDemuxPacket.h"
#include "kodi/util/XMLUtils.h"
#include <p8-platform/util/StringUtils.h>
#include "xbmc_codec_descriptor.hpp"
#include "RE.h"

using namespace std;
using namespace ADDON;

static bool m_IsStoped;
extern ADDON::CHelper_libXBMC_addon *XBMC;

RE::RE(void)
{
  m_iEpgStart = -1;
  m_strDefaultIcon =  "http://www.royalty-free.tv/news/wp-content/uploads/2011/06/cc-logo1.jpg";
  m_strDefaultMovie = "";
  
  XBMC->Log(LOG_DEBUG, "%s - Send command to INIT, command send as successful (%s).", __FUNCTION__, RE::SendDVBCMD("CMD_INIT") ? "true" : "false");
  XBMC->Log(LOG_DEBUG, "%s - Create DVBServer lister.", __FUNCTION__);
  m_lDVBServer = pthread_create(&m_lDVBServer, NULL, RE::DVBServer, NULL);
  XBMC->Log(LOG_DEBUG, "%s - Created DVBServer lister.", __FUNCTION__);

  LoadDemoData();
}

RE::~RE(void)
{
  XBMC->Log(LOG_DEBUG, "%s - Send command to TERM, command send as successful (%s).", __FUNCTION__, RE::SendDVBCMD("CMD_TERM") ? "true" : "false");
  XBMC->Log(LOG_DEBUG, "%s - Change to stop DVBServer lister.", __FUNCTION__);
  m_IsStoped = true;
  XBMC->Log(LOG_DEBUG, "%s - Changed to DVBServer lister.", __FUNCTION__);
  m_streams.Clear();
  m_streamStat.clear();
  m_channels.clear();
  m_groups.clear();
}

std::string RE::GetSettingsFile() const
{
  /*string settingFile = g_strClientPath;
  if (settingFile.at(settingFile.size() - 1) == '\\' ||
      settingFile.at(settingFile.size() - 1) == '/')
    settingFile.append("REAddonSettings.xml");
  else
    settingFile.append("/REAddonSettings.xml");*/
  return "/data/chandata/db/chandb.xml";
}

bool RE::LoadDemoData(void)
{
  TiXmlDocument xmlDoc;
  string strSettingsFile = GetSettingsFile();

  if (!xmlDoc.LoadFile(strSettingsFile))
  {
    XBMC->Log(LOG_ERROR, "invalid demo data (no/invalid data file found at '%s')", strSettingsFile.c_str());
    return false;
  }

  TiXmlElement *pRootElement = xmlDoc.RootElement();
  if (strcmp(pRootElement->Value(), "satellites") != 0)
  {
    XBMC->Log(LOG_ERROR, "invalid demo data (no <demo> tag found)");
    return false;
  }

  /* load channels */
  XBMC->Log(LOG_DEBUG, "%s - Loading channels.", __FUNCTION__);
  TiXmlNode *pSATNode = NULL;
  while ((pSATNode = pRootElement->IterateChildren(pSATNode)) != NULL)
  {
	TiXmlNode *pTransponderNode = NULL;
	while ((pTransponderNode = pSATNode->IterateChildren(pTransponderNode)) != NULL)
    {
	  TiXmlNode *pE2Node = NULL;
      while ((pE2Node = pTransponderNode->IterateChildren(pE2Node)) != NULL)
	  {
		REChannel channel;
		TiXmlElement* pE2Element = pE2Node->ToElement();
		
		channel.bRadio              = false;
		channel.iCaid               = atoi(pE2Element->Attribute("caid"));
		channel.iTuner              = atoi(pE2Element->Attribute("tuner_id"));
		channel.iUniqueId           = atoi(pE2Element->Attribute("idx"));
		channel.iChannelNumber      = atoi(pE2Element->Attribute("chan_no"));
		channel.iSubChannelNumber   = channel.iChannelNumber;
		channel.iEncryptionSystem   = 0;
		channel.strChannelName      = pE2Element->FirstChildElement("e2servicename")->GetText();
		channel.strIconPath         = "";
		channel.strStreamURL        = "";
		channel.strServiceReference = pE2Element->FirstChildElement("e2servicereference")->GetText();
		
		TiXmlElement *pPIDSElement = pE2Element->FirstChildElement("pids");
		TiXmlNode *pPIDNode = NULL;
		while ((pPIDNode = pPIDSElement->IterateChildren(pPIDNode)) != NULL)
		{
		  REStreamInfo stream;
		  TiXmlElement* pPIDElement = pPIDNode->ToElement();
		  stream.val    = atoi(pPIDElement->Attribute("val"));
		  stream.type   = pPIDElement->Attribute("type");
		  if (stream.type == "audio" || stream.type == "video")
		  {
			stream.format = pPIDElement->Attribute("format");
			if (stream.type == "video" && stream.format == "UNKNOWN")
		  	  channel.bRadio = true;
		  }
		  else if (stream.type == "subt")
			stream.format = "TEXT";
		  else
		   stream.format = "";
		  if (stream.type == "audio")
			stream.lang   = pPIDElement->Attribute("lang");
		  else
		    stream.lang   = "";
		  if (stream.val > 0)
		    channel.streams.push_back(stream);
		}
		m_channels.push_back(channel);
	  }
	}
  }
  XBMC->Log(LOG_DEBUG, "%s - Finished total(%d).", __FUNCTION__, m_channels.size());
/*
  // load channel groups 
  int iUniqueGroupId = 0;
  pElement = pRootElement->FirstChildElement("grouplist");
  if (pElement)
  {
    TiXmlNode *pGroupNode = NULL;
    while ((pGroupNode = pElement->IterateChildren(pGroupNode)) != NULL)
    {
      CStdString strTmp;
      REChannelGroup group;
      group.iGroupId = ++iUniqueGroupId;

      // group name 
      if (!XMLUtils::GetString(pGroupNode, "groupname", strTmp))
        continue;
      group.strGroupName = strTmp;

      // radio/TV 
      XMLUtils::GetBoolean(pGroupNode, "radio", group.bRadio);
      
      // sort position 
      XMLUtils::GetInt(pGroupNode, "position", group.iPosition);

      // members 
      TiXmlNode* pMembers = pGroupNode->FirstChild("members");
      TiXmlNode *pMemberNode = NULL;
      while (pMembers != NULL && (pMemberNode = pMembers->IterateChildren(pMemberNode)) != NULL)
      {
        int iChannelId = atoi(pMemberNode->FirstChild()->Value());
        if (iChannelId > -1)
          group.members.push_back(iChannelId);
      }

      m_groups.push_back(group);
    }
  }

  // load EPG entries 
  pElement = pRootElement->FirstChildElement("epg");
  if (pElement)
  {
    TiXmlNode *pEpgNode = NULL;
    while ((pEpgNode = pElement->IterateChildren(pEpgNode)) != NULL)
    {
      CStdString strTmp;
      int iTmp;
      REEpgEntry entry;

      // broadcast id 
      if (!XMLUtils::GetInt(pEpgNode, "broadcastid", entry.iBroadcastId))
        continue;

      // channel id 
      if (!XMLUtils::GetInt(pEpgNode, "channelid", iTmp))
        continue;
      REChannel &channel = m_channels.at(iTmp - 1);
      entry.iChannelId = channel.iUniqueId;

      // title 
      if (!XMLUtils::GetString(pEpgNode, "title", strTmp))
        continue;
      entry.strTitle = strTmp;

      // start 
      if (!XMLUtils::GetInt(pEpgNode, "start", iTmp))
        continue;
      entry.startTime = iTmp;

      // end 
      if (!XMLUtils::GetInt(pEpgNode, "end", iTmp))
        continue;
      entry.endTime = iTmp;

      // plot 
      if (XMLUtils::GetString(pEpgNode, "plot", strTmp))
        entry.strPlot = strTmp;

      // plot outline 
      if (XMLUtils::GetString(pEpgNode, "plotoutline", strTmp))
        entry.strPlotOutline = strTmp;

      // icon path 
      if (XMLUtils::GetString(pEpgNode, "icon", strTmp))
        entry.strIconPath = strTmp;

      // genre type 
      XMLUtils::GetInt(pEpgNode, "genretype", entry.iGenreType);

      // genre subtype 
      XMLUtils::GetInt(pEpgNode, "genresubtype", entry.iGenreSubType);

      XBMC->Log(LOG_DEBUG, "loaded EPG entry '%s' channel '%d' start '%d' end '%d'", entry.strTitle.c_str(), entry.iChannelId, entry.startTime, entry.endTime);
      channel.epg.push_back(entry);
    }
  }

  // load recordings 
  iUniqueGroupId = 0; // reset unique ids
  pElement = pRootElement->FirstChildElement("recordings");
  if (pElement)
  {
    TiXmlNode *pRecordingNode = NULL;
    while ((pRecordingNode = pElement->IterateChildren(pRecordingNode)) != NULL)
    {
      CStdString strTmp;
      RERecording recording;

      // recording title 
      if (!XMLUtils::GetString(pRecordingNode, "title", strTmp))
        continue;
      recording.strTitle = strTmp;

      // recording url 
      if (!XMLUtils::GetString(pRecordingNode, "url", strTmp))
        recording.strStreamURL = m_strDefaultMovie;
      else
        recording.strStreamURL = strTmp;

      // recording path 
      if (XMLUtils::GetString(pRecordingNode, "directory", strTmp))
        recording.strDirectory = strTmp;

      iUniqueGroupId++;
      strTmp.Format("%d", iUniqueGroupId);
      recording.strRecordingId = strTmp;

      // channel name 
      if (XMLUtils::GetString(pRecordingNode, "channelname", strTmp))
        recording.strChannelName = strTmp;

      // plot 
      if (XMLUtils::GetString(pRecordingNode, "plot", strTmp))
        recording.strPlot = strTmp;

      // plot outline 
      if (XMLUtils::GetString(pRecordingNode, "plotoutline", strTmp))
        recording.strPlotOutline = strTmp;

      // genre type 
      XMLUtils::GetInt(pRecordingNode, "genretype", recording.iGenreType);

      // genre subtype 
      XMLUtils::GetInt(pRecordingNode, "genresubtype", recording.iGenreSubType);

      // duration 
      XMLUtils::GetInt(pRecordingNode, "duration", recording.iDuration);

      // recording time 
      if (XMLUtils::GetString(pRecordingNode, "time", strTmp))
      {
        time_t timeNow = time(NULL);
        struct tm* now = localtime(&timeNow);

        CStdString::size_type delim = strTmp.Find(':');
        if (delim != CStdString::npos)
        {
          now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
          now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
          now->tm_mday--; // yesterday

          recording.recordingTime = mktime(now);
        }
      }

      m_recordings.push_back(recording);
    }
  }

  // load deleted recordings 
  pElement = pRootElement->FirstChildElement("recordingsdeleted");
  if (pElement)
  {
    TiXmlNode *pRecordingNode = NULL;
    while ((pRecordingNode = pElement->IterateChildren(pRecordingNode)) != NULL)
    {
      CStdString strTmp;
      RERecording recording;

      // recording title 
      if (!XMLUtils::GetString(pRecordingNode, "title", strTmp))
        continue;
      recording.strTitle = strTmp;

      // recording url 
      if (!XMLUtils::GetString(pRecordingNode, "url", strTmp))
        recording.strStreamURL = m_strDefaultMovie;
      else
        recording.strStreamURL = strTmp;

      // recording path 
      if (XMLUtils::GetString(pRecordingNode, "directory", strTmp))
        recording.strDirectory = strTmp;

      iUniqueGroupId++;
      strTmp.Format("%d", iUniqueGroupId);
      recording.strRecordingId = strTmp;

      // channel name 
      if (XMLUtils::GetString(pRecordingNode, "channelname", strTmp))
        recording.strChannelName = strTmp;

      // plot 
      if (XMLUtils::GetString(pRecordingNode, "plot", strTmp))
        recording.strPlot = strTmp;

      // plot outline 
      if (XMLUtils::GetString(pRecordingNode, "plotoutline", strTmp))
        recording.strPlotOutline = strTmp;

      // genre type 
      XMLUtils::GetInt(pRecordingNode, "genretype", recording.iGenreType);

      // genre subtype 
      XMLUtils::GetInt(pRecordingNode, "genresubtype", recording.iGenreSubType);

      // duration 
      XMLUtils::GetInt(pRecordingNode, "duration", recording.iDuration);

      // recording time 
      if (XMLUtils::GetString(pRecordingNode, "time", strTmp))
      {
        time_t timeNow = time(NULL);
        struct tm* now = localtime(&timeNow);

        CStdString::size_type delim = strTmp.Find(':');
        if (delim != CStdString::npos)
        {
          now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
          now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
          now->tm_mday--; // yesterday

          recording.recordingTime = mktime(now);
        }
      }

      m_recordingsDeleted.push_back(recording);
    }
  }

  // load timers 
  pElement = pRootElement->FirstChildElement("timers");
  if (pElement)
  {
    TiXmlNode *pTimerNode = NULL;
    while ((pTimerNode = pElement->IterateChildren(pTimerNode)) != NULL)
    {
      CStdString strTmp;
      int iTmp;
      RETimer timer;
      time_t timeNow = time(NULL);
      struct tm* now = localtime(&timeNow);

      // channel id 
      if (!XMLUtils::GetInt(pTimerNode, "channelid", iTmp))
        continue;
      REChannel &channel = m_channels.at(iTmp - 1);
      timer.iChannelId = channel.iUniqueId;

      // state 
      if (XMLUtils::GetInt(pTimerNode, "state", iTmp))
        timer.state = (PVR_TIMER_STATE) iTmp;

      // title 
      if (!XMLUtils::GetString(pTimerNode, "title", strTmp))
        continue;
      timer.strTitle = strTmp;

      // summary 
      if (!XMLUtils::GetString(pTimerNode, "summary", strTmp))
        continue;
      timer.strSummary = strTmp;

      // start time 
      if (XMLUtils::GetString(pTimerNode, "starttime", strTmp))
      {
        CStdString::size_type delim = strTmp.Find(':');
        if (delim != CStdString::npos)
        {
          now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
          now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);

          timer.startTime = mktime(now);
        }
      }

      // end time 
      if (XMLUtils::GetString(pTimerNode, "endtime", strTmp))
      {
        CStdString::size_type delim = strTmp.Find(':');
        if (delim != CStdString::npos)
        {
          now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
          now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);

          timer.endTime = mktime(now);
        }
      }

      XBMC->Log(LOG_DEBUG, "loaded timer '%s' channel '%d' start '%d' end '%d'", timer.strTitle.c_str(), timer.iChannelId, timer.startTime, timer.endTime);
      m_timers.push_back(timer);
    }
  }
*/
  return true;
}

bool RE::SendDVBCMD(std::string cmd)
{
  int sock;
  struct sockaddr_in server;
  char server_reply[1024];
   
  //Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1)
    XBMC->Log(LOG_DEBUG, "%s - Could not create socket.", __FUNCTION__);
  else
  {
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(4000);
    
    //Connect to remote server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
	  XBMC->Log(LOG_DEBUG, "%s - Connect failed.", __FUNCTION__);
    else
	{
	  //Send some data
	  if (send(sock, cmd.c_str(), strlen(cmd.c_str()), 0) < 0)
	    XBMC->Log(LOG_DEBUG, "%s - Send cmd failed.", __FUNCTION__);
      else
	  {
		if (recv(sock, server_reply, 1024, 0) < 0)
		  XBMC->Log(LOG_DEBUG, "%s - recv failed.", __FUNCTION__);
	    else
		{
		  close(sock);
		  XBMC->Log(LOG_DEBUG, "%s - Server reply(%s) CMD(%s).", __FUNCTION__, server_reply, cmd.c_str());
		  return (string(server_reply) == "STAT_OK");
		}
	  }
	  close(sock);
	}
  }
  return false;
}

void *RE::DVBServer(void *)
{
  int socket_desc, client_sock, c, *new_sock;
  struct sockaddr_in server, client;
   
  //Create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc == -1)
	XBMC->Log(LOG_DEBUG, "%s - Could not create socket", __FUNCTION__);
  else
  {
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(4001);
	//Bind
	if(bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	  XBMC->Log(LOG_DEBUG, "%s - Socket bind failed.", __FUNCTION__);
	else
	{
	  //Listen
	  listen(socket_desc, 3);
	  //Accept and incoming connection
	  XBMC->Log(LOG_DEBUG, "%s - Waiting for incoming connections...", __FUNCTION__);
	  c = sizeof(struct sockaddr_in);
	  while((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) && !m_IsStoped)
	  {
		XBMC->Log(LOG_DEBUG, "%s - Connection accepted.", __FUNCTION__);
		pthread_t sniffer_thread;
		new_sock = (int *)malloc(1);
		*new_sock = client_sock;
		 
		if(pthread_create(&sniffer_thread, NULL, RE::CDVBServer, (void*)new_sock) < 0)
		{
		  XBMC->Log(LOG_DEBUG, "%s - Could not create thread.", __FUNCTION__);
		  break;
		}
	  }
	}
  }

  if (client_sock < 0)
    XBMC->Log(LOG_DEBUG, "%s - Accept failed.", __FUNCTION__);
}

void *RE::CDVBServer(void *socket_desc)
{
  //Get the socket descriptor
  int sock = *(int*)socket_desc;
  int read_size;
  char client_message[1024];
   
  //Receive a message from client
  while((read_size = recv(sock, &client_message, 1024, 0)) > 0 && !m_IsStoped)
  {
	XBMC->Log(LOG_DEBUG, "%s - %s", __FUNCTION__, client_message);
	/*if (!strncmp("EVT_LIVE_CHAN_START_OK", client_message, 22))
    {
	  std::string dados = std::string(client_message);
      RE::GetInstance().ParseXMLChannelToStream(dados.substr(dados.find("<?")));
    }*/
  }
  
  if(read_size == -1)
	XBMC->Log(LOG_DEBUG, "%s - recv failed.", __FUNCTION__);
       
  //Free the socket pointer
  free(socket_desc);
}

void RE::ParseXMLChannelToStream(std::string dados)
{
  XBMC->Log(LOG_DEBUG, "%s - Called.", __FUNCTION__);
  vector<XbmcPvrStream>  streams;
  m_streamStat.clear();
  
  TiXmlDocument xmlDoc;
  if (!xmlDoc.Parse(dados.c_str(), 0, TIXML_ENCODING_UTF8))
  {
    XBMC->Log(LOG_ERROR, "%s - Not is passible pass dados(%s).", __FUNCTION__, dados.c_str());
    return;
  }
  
  TiXmlElement *pElement = xmlDoc.FirstChildElement("pids");
  if (pElement)
  {
	//TiXmlNode *pChannelNode = NULL;
	for (TiXmlElement *pid = pElement->FirstChildElement("pid"); pid; pid = pElement->NextSiblingElement())
    //while ((pChannelNode = pElement->IterateChildren(pChannelNode)) != NULL)
    {
      CStdString    strTmp;
	  uint32_t      idx;
      XbmcPvrStream stream;
		
	  /* Find stream */
      m_streamStat[idx] = 0;
      m_streams.GetStreamData(idx, &stream);
      XBMC->Log(LOG_DEBUG, "%s - Demux subscription start.", __FUNCTION__);
	  
      /* Type */
      //if (!XMLUtils::GetString(pChannelNode, "type", strTmp))
      //  continue;
	  XBMC->Log(LOG_DEBUG, "%s - type(%s)", __FUNCTION__, pid->Attribute("type"));
	  
	  /* Format */
	  //if (!XMLUtils::GetString(pChannelNode, "format", strTmp))
      //  continue;
	  strTmp = pid->Attribute("format");
	  if (strTmp.substr(strTmp.size()-4) == std::string("_AUD"))
		strTmp = strTmp.substr(0, strTmp.size() - 4);
	  else if (strTmp.substr(strTmp.size()-4) == std::string("_VID"))
		strTmp = strTmp.substr(0, strTmp.size() - 4) + "VIDEO";
	  XBMC->Log(LOG_DEBUG, "%s - Format(%s)", __FUNCTION__, strTmp.c_str());
	  
	  CodecDescriptor codecDescriptor = CodecDescriptor::GetCodecByName(strTmp.c_str());
	  xbmc_codec_t codec = codecDescriptor.Codec();
	  
	  XBMC->Log(LOG_DEBUG, "%s - XBMC_CODEC_TYPE_UNKNOWN(%s)", __FUNCTION__, codec.codec_type == XBMC_CODEC_TYPE_UNKNOWN ? "true" : "false");
	  if (codec.codec_type != XBMC_CODEC_TYPE_UNKNOWN)
      {
        stream.iCodecType  = codec.codec_type;
        stream.iCodecId    = codec.codec_id;
        stream.iPhysicalId = idx;
      
        /* Subtitle ID */
        if (stream.iCodecType == XBMC_CODEC_TYPE_SUBTITLE)
        {
          stream.iIdentifier = -1;
        }
      
        /* Language */
        if (stream.iCodecType == XBMC_CODEC_TYPE_SUBTITLE || stream.iCodecType == XBMC_CODEC_TYPE_AUDIO)
        {
          //if (!XMLUtils::GetString(pChannelNode, "lang", strTmp))
          //  continue;
		  strTmp = pid->Attribute("lang");
	      XBMC->Log(LOG_DEBUG, "%s - Lang(%s)", __FUNCTION__, strTmp.c_str());
          strncpy(stream.strLanguage, strTmp.c_str(), sizeof(stream.strLanguage) - 1);
        }
      
        /* Audio data */
        if (stream.iCodecType == XBMC_CODEC_TYPE_AUDIO)
        {
          stream.iChannels  += 1;
          stream.iSampleRate = 48000;
        }
      
        /* Video */
        if (stream.iCodecType == XBMC_CODEC_TYPE_VIDEO)
        {
		  XBMC->Log(LOG_DEBUG, "%s - video.", __FUNCTION__);
          stream.iWidth   = 640;
          stream.iHeight  = 480;
          
          /* Setting aspect ratio to zero will cause XBMC to handle changes in it */
          stream.fAspect   = 1.4859f;
          stream.iFPSScale = 1000;
          stream.iFPSRate  = 29970;
        }
          
        streams.push_back(stream);
        XBMC->Log(LOG_DEBUG, "%s - id(%d) codec(%u)", __FUNCTION__, idx, stream.iCodecId);
      }
    }
  }
  
  /* Update streams */
  XBMC->Log(LOG_DEBUG, "%s - Demux stream change.", __FUNCTION__);
  m_streams.UpdateStreams(streams);
}

int RE::GetChannelsAmount(void)
{
  return m_channels.size();
}

PVR_ERROR RE::GetChannels(ADDON_HANDLE handle, bool bRadio)
{
  for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
  {
    REChannel &channel = m_channels.at(iChannelPtr);
    if (channel.bRadio == bRadio)
    {
      PVR_CHANNEL xbmcChannel;
      memset(&xbmcChannel, 0, sizeof(PVR_CHANNEL));

      xbmcChannel.iUniqueId         = channel.iUniqueId;
      xbmcChannel.bIsRadio          = channel.bRadio;
      xbmcChannel.iChannelNumber    = channel.iChannelNumber;
      xbmcChannel.iSubChannelNumber = channel.iSubChannelNumber;
      strncpy(xbmcChannel.strChannelName, channel.strChannelName.c_str(), sizeof(xbmcChannel.strChannelName) - 1);
      strncpy(xbmcChannel.strStreamURL, channel.strStreamURL.c_str(), sizeof(xbmcChannel.strStreamURL) - 1);
      xbmcChannel.iEncryptionSystem = channel.iEncryptionSystem;
      strncpy(xbmcChannel.strIconPath, channel.strIconPath.c_str(), sizeof(xbmcChannel.strIconPath) - 1);
      xbmcChannel.bIsHidden         = false;

      PVR->TransferChannelEntry(handle, &xbmcChannel);
    }
  }

  return PVR_ERROR_NO_ERROR;
}

bool RE::GetChannel(const PVR_CHANNEL &channel, REChannel &myChannel)
{ 
  for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
  {
    REChannel &thisChannel = m_channels.at(iChannelPtr);
    if (thisChannel.iUniqueId == (int) channel.iUniqueId)
    {
      //myChannel.iUniqueId                = thisChannel.iUniqueId;
      //myChannel.bRadio                   = thisChannel.bRadio;
	  //myChannel.iCaid                    = thisChannel.iCaid;
      //myChannel.iChannelNumber           = thisChannel.iChannelNumber;
      //myChannel.iSubChannelNumber        = thisChannel.iSubChannelNumber;
      //myChannel.iEncryptionSystem        = thisChannel.iEncryptionSystem;
      //myChannel.strChannelName           = thisChannel.strChannelName;
      //myChannel.strIconPath              = thisChannel.strIconPath;
      //myChannel.strStreamURL             = thisChannel.strStreamURL;
	  //myChannel.strServiceReference      = thisChannel.strServiceReference;
	  myChannel = thisChannel;
      return true;
    }
  }

  return false;
}

PVR_ERROR RE::GetStreamProperties(PVR_STREAM_PROPERTIES *props, REChannel channel)
{
  XBMC->Log(LOG_DEBUG, "%s - Called.", __FUNCTION__);
  vector<XbmcPvrStream> streams;
  m_streamStat.clear();

  for (unsigned int iEntryPtr = 0; iEntryPtr < channel.streams.size(); iEntryPtr++)
  {
    REStreamInfo &myStream = channel.streams.at(iEntryPtr);
	if (myStream.val > 0)
	{
      /* Find stream */
	  XbmcPvrStream stream;
      m_streamStat[myStream.val] = 0;
	  m_streams.GetStreamData(myStream.val, &stream);
      XBMC->Log(LOG_DEBUG, "%s - Demux subscription start.", __FUNCTION__);
	  
	  CodecDescriptor codecDescriptor = CodecDescriptor::GetCodecByName(myStream.format.c_str());
	  xbmc_codec_t codec = codecDescriptor.Codec();
	  
	  XBMC->Log(LOG_DEBUG, "%s - type(%s)", __FUNCTION__, myStream.type.c_str());
	  if (codec.codec_type != XBMC_CODEC_TYPE_UNKNOWN && codec.codec_type != XBMC_CODEC_TYPE_SUBTITLE)
	  {
		stream.iPhysicalId= myStream.val;
	    stream.iCodecType = codec.codec_type;
        stream.iCodecId   = codec.codec_id;
		stream.iIdentifier= -1;
	  
	    if (codec.codec_type == XBMC_CODEC_TYPE_AUDIO) /* Language */
        {
          const char *language = myStream.lang.c_str();
        
          stream.iChannels      = 1;
          stream.iSampleRate    = 48000;
          stream.iBlockAlign    = 0;
          stream.iBitRate       = 0;
          stream.iBitsPerSample = 0;
          stream.strLanguage[0] = language[0];
          stream.strLanguage[1] = language[1];
          stream.strLanguage[2] = language[2];
          stream.strLanguage[3] = 0;
        }
        else if (codec.codec_type == XBMC_CODEC_TYPE_VIDEO)
        {
          stream.iFPSScale      = 1000;
          stream.iFPSRate       = 29970;
          stream.iHeight        = 1080;
          stream.iWidth         = 1920;
          stream.fAspect        = 0.0f;
          stream.strLanguage[0] = 0;
          stream.strLanguage[1] = 0;
          stream.strLanguage[2] = 0;
          stream.strLanguage[3] = 0;
        }
        /*else if (codec.codec_type == XBMC_CODEC_TYPE_SUBTITLE)
        {
          const char *language = resp->extract_String();
          uint32_t composition_id = resp->extract_U32();
          uint32_t ancillary_id = resp->extract_U32();
          stream.strLanguage[0] = language[0];
          stream.strLanguage[1] = language[1];
          stream.strLanguage[2] = language[2];
          stream.strLanguage[3] = 0;
          stream.iSubtitleInfo = (composition_id & 0xffff) | ((ancillary_id & 0xffff) << 16);
        }
        else if (codec.codec_type == XBMC_CODEC_TYPE_RDS)
        {
          const char *language = resp->extract_String();
          uint32_t rel_channel_pid = resp->extract_U32();
          stream.strLanguage[0] = language[0];
          stream.strLanguage[1] = language[1];
          stream.strLanguage[2] = language[2];
          stream.strLanguage[3] = 0;
        }*/
	    streams.push_back(stream);
		XBMC->Log(LOG_DEBUG, "%s - id: %d, type %s, codec: (%u-%s)", __FUNCTION__, myStream.val, myStream.type.c_str(), stream.iCodecId, myStream.format.c_str());
	  }
	}
  }
  
  /* Update streams */
  XBMC->Log(LOG_DEBUG, "%s - demux stream update change.", __FUNCTION__);
  m_streams.UpdateStreams(streams);
  return m_streams.GetProperties(props) ? PVR_ERROR_NO_ERROR : PVR_ERROR_SERVER_ERROR;
}

int RE::GetChannelGroupsAmount(void)
{
  return m_groups.size();
}

PVR_ERROR RE::GetChannelGroups(ADDON_HANDLE handle, bool bRadio)
{
  for (unsigned int iGroupPtr = 0; iGroupPtr < m_groups.size(); iGroupPtr++)
  {
    REChannelGroup &group = m_groups.at(iGroupPtr);
    if (group.bRadio == bRadio)
    {
      PVR_CHANNEL_GROUP xbmcGroup;
      memset(&xbmcGroup, 0, sizeof(PVR_CHANNEL_GROUP));

      xbmcGroup.bIsRadio = bRadio;
      xbmcGroup.iPosition = group.iPosition;
      strncpy(xbmcGroup.strGroupName, group.strGroupName.c_str(), sizeof(xbmcGroup.strGroupName) - 1);

      PVR->TransferChannelGroup(handle, &xbmcGroup);
    }
  }

  return PVR_ERROR_NO_ERROR;
}

PVR_ERROR RE::GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP &group)
{
  for (unsigned int iGroupPtr = 0; iGroupPtr < m_groups.size(); iGroupPtr++)
  {
    REChannelGroup &myGroup = m_groups.at(iGroupPtr);
    if (!strcmp(myGroup.strGroupName.c_str(),group.strGroupName))
    {
      for (unsigned int iChannelPtr = 0; iChannelPtr < myGroup.members.size(); iChannelPtr++)
      {
        int iId = myGroup.members.at(iChannelPtr) - 1;
        if (iId < 0 || iId > (int)m_channels.size() - 1)
          continue;
        REChannel &channel = m_channels.at(iId);
        PVR_CHANNEL_GROUP_MEMBER xbmcGroupMember;
        memset(&xbmcGroupMember, 0, sizeof(PVR_CHANNEL_GROUP_MEMBER));

        strncpy(xbmcGroupMember.strGroupName, group.strGroupName, sizeof(xbmcGroupMember.strGroupName) - 1);
        xbmcGroupMember.iChannelUniqueId  = channel.iUniqueId;
        xbmcGroupMember.iChannelNumber    = channel.iChannelNumber;

        PVR->TransferChannelGroupMember(handle, &xbmcGroupMember);
      }
    }
  }

  return PVR_ERROR_NO_ERROR;
}

PVR_ERROR RE::GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL &channel, time_t iStart, time_t iEnd)
{
  if (m_iEpgStart == -1)
    m_iEpgStart = iStart;

  time_t iLastEndTime = m_iEpgStart + 1;
  int iAddBroadcastId = 0;

  for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
  {
    REChannel &myChannel = m_channels.at(iChannelPtr);
    if (myChannel.iUniqueId != (int) channel.iUniqueId)
      continue;

    while (iLastEndTime < iEnd && myChannel.epg.size() > 0)
    {
      time_t iLastEndTimeTmp = 0;
      for (unsigned int iEntryPtr = 0; iEntryPtr < myChannel.epg.size(); iEntryPtr++)
      {
        REEpgEntry &myTag = myChannel.epg.at(iEntryPtr);

        EPG_TAG tag;
        memset(&tag, 0, sizeof(EPG_TAG));

        tag.iUniqueBroadcastId = myTag.iBroadcastId + iAddBroadcastId;
        tag.strTitle           = myTag.strTitle.c_str();
        tag.iChannelNumber     = myTag.iChannelId;
        tag.startTime          = myTag.startTime + iLastEndTime;
        tag.endTime            = myTag.endTime + iLastEndTime;
        tag.strPlotOutline     = myTag.strPlotOutline.c_str();
        tag.strPlot            = myTag.strPlot.c_str();
        tag.strIconPath        = myTag.strIconPath.c_str();
        tag.iGenreType         = myTag.iGenreType;
        tag.iGenreSubType      = myTag.iGenreSubType;
        tag.iFlags             = EPG_TAG_FLAG_UNDEFINED;
        
        iLastEndTimeTmp = tag.endTime;

        PVR->TransferEpgEntry(handle, &tag);
      }

      iLastEndTime = iLastEndTimeTmp;
      iAddBroadcastId += myChannel.epg.size();
    }
  }

  return PVR_ERROR_NO_ERROR;
}

int RE::GetRecordingsAmount(bool bDeleted)
{
  return bDeleted ? m_recordingsDeleted.size() : m_recordings.size();
}

PVR_ERROR RE::GetRecordings(ADDON_HANDLE handle, bool bDeleted)
{
  std::vector<RERecording> *recordings = bDeleted ? &m_recordingsDeleted : &m_recordings;

  for (std::vector<RERecording>::iterator it = recordings->begin() ; it != recordings->end() ; it++)
  {
    RERecording &recording = *it;

    PVR_RECORDING xbmcRecording;

    xbmcRecording.iDuration     = recording.iDuration;
    xbmcRecording.iGenreType    = recording.iGenreType;
    xbmcRecording.iGenreSubType = recording.iGenreSubType;
    xbmcRecording.recordingTime = recording.recordingTime;
    xbmcRecording.bIsDeleted      = bDeleted;

    strncpy(xbmcRecording.strChannelName, recording.strChannelName.c_str(), sizeof(xbmcRecording.strChannelName) - 1);
    strncpy(xbmcRecording.strPlotOutline, recording.strPlotOutline.c_str(), sizeof(xbmcRecording.strPlotOutline) - 1);
    strncpy(xbmcRecording.strPlot,        recording.strPlot.c_str(),        sizeof(xbmcRecording.strPlot) - 1);
    strncpy(xbmcRecording.strRecordingId, recording.strRecordingId.c_str(), sizeof(xbmcRecording.strRecordingId) - 1);
    strncpy(xbmcRecording.strTitle,       recording.strTitle.c_str(),       sizeof(xbmcRecording.strTitle) - 1);
    strncpy(xbmcRecording.strStreamURL,   recording.strStreamURL.c_str(),   sizeof(xbmcRecording.strStreamURL) - 1);
    strncpy(xbmcRecording.strDirectory,   recording.strDirectory.c_str(),   sizeof(xbmcRecording.strDirectory) - 1);

    PVR->TransferRecordingEntry(handle, &xbmcRecording);
  }

  return PVR_ERROR_NO_ERROR;
}

int RE::GetTimersAmount(void)
{
  return m_timers.size();
}

PVR_ERROR RE::GetTimers(ADDON_HANDLE handle)
{
  unsigned int i = PVR_TIMER_NO_CLIENT_INDEX + 1;
  for (std::vector<RETimer>::iterator it = m_timers.begin() ; it != m_timers.end() ; it++)
  {
    RETimer &timer = *it;

    PVR_TIMER xbmcTimer;
    memset(&xbmcTimer, 0, sizeof(PVR_TIMER));

    /* TODO: Implement own timer types to get support for the timer features introduced with PVR API 1.9.7 */
    xbmcTimer.iTimerType = PVR_TIMER_TYPE_NONE;

    xbmcTimer.iClientIndex      = i++;
    xbmcTimer.iClientChannelUid = timer.iChannelId;
    xbmcTimer.startTime         = timer.startTime;
    xbmcTimer.endTime           = timer.endTime;
    xbmcTimer.state             = timer.state;

    strncpy(xbmcTimer.strTitle, timer.strTitle.c_str(), sizeof(timer.strTitle) - 1);
    strncpy(xbmcTimer.strSummary, timer.strSummary.c_str(), sizeof(timer.strSummary) - 1);

    PVR->TransferTimerEntry(handle, &xbmcTimer);
  }

  return PVR_ERROR_NO_ERROR;
}

bool RE::OpenLiveStream(REChannel channel)
{
  if (RE::SendDVBCMD(StringUtils::Format("CMD_START_LIVE_REFID:0,%d,%s", channel.iTuner, channel.strServiceReference.c_str())))
  {
    std::ofstream frontpanel("/dev/front");  
    if (!frontpanel) 
      XBMC->Log(LOG_DEBUG, "%s - Not is possible write channel number in front panel.", __FUNCTION__);
    else
      frontpanel<<StringUtils::Format("%04d", channel.iChannelNumber).c_str();
    return true;
  }
  return false;
}

void RE::CloseLiveStream(REChannel channel)
{
	RE::SendDVBCMD(StringUtils::Format("CMD_STOP_LIVE:0,%d", channel.iTuner));
}

DemuxPacket* RE::DemuxRead(REChannel channel)
{
  return NULL;
  XBMC->Log(LOG_DEBUG, "%s - Called.", __FUNCTION__);
  REStreamInfo &myStream = channel.streams.at(0);
  DemuxPacket* pPacket = {};
  if (!pPacket)
  {
	XBMC->Log(LOG_ERROR, "%s - DemuxPacket not created.", __FUNCTION__);
	return NULL;
  }
  try
  {
    memset(pPacket, 0, sizeof(DemuxPacket));

    /*if (iDataSize > 0)
    {
      // need to allocate a few bytes more.
      // From avcodec.h (ffmpeg)
      //Required number of additionally allocated bytes at the end of the input bitstream for decoding.
      //this is mainly needed because some optimized bitstream readers read
      //32 or 64 bit at once and could read over the end<br>
      //Note, if the first 23 bits of the additional bytes are not 0 then damaged
      //MPEG bitstreams could cause overread and segfault
      pPacket->pData =(uint8_t*)_aligned_malloc(iDataSize + FF_INPUT_BUFFER_PADDING_SIZE, 16);
      if (!pPacket->pData)
      {
        FreeDemuxPacket(pPacket);
        return NULL;
      }

      // reset the last 8 bytes to 0;
      memset(pPacket->pData + iDataSize, 0, FF_INPUT_BUFFER_PADDING_SIZE);
    }*/

    // setup defaults
    pPacket->dts       = DVD_NOPTS_VALUE;
    pPacket->pts       = DVD_NOPTS_VALUE;
    pPacket->iStreamId = myStream.val;
  }
  catch(...)
  {
    XBMC->Log(LOG_ERROR, "%s - Exception thrown.", __FUNCTION__);
    pPacket = NULL;
  }

  XBMC->Log(LOG_DEBUG, "%s - nome(%s) tipo(%s) idx(%d).", __FUNCTION__, channel.strChannelName.c_str(), myStream.type.c_str(), myStream.val);
  return pPacket;
}