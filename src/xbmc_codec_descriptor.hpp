/*
 *      Copyright (C) 2005-2013 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */
#ifndef XBMC_CODEC_DESCRIPTOR_HPP
#define	XBMC_CODEC_DESCRIPTOR_HPP

#include "kodi/libXBMC_codec.h"
#include "client.h"

/**
 * Adapter which converts codec names used by tvheadend and VDR into their 
 * FFmpeg equivalents.
 */
class CodecDescriptor
{
public:
  CodecDescriptor(void)
  {
    m_codec.codec_id   = XBMC_INVALID_CODEC_ID;
    m_codec.codec_type = XBMC_CODEC_TYPE_UNKNOWN;
  }

  CodecDescriptor(xbmc_codec_t codec, const char* name) :
    m_codec(codec),
    m_strName(name) {}
  virtual ~CodecDescriptor(void) {}

  xbmc_codec_t Codec(void) const { return m_codec; }

  static CodecDescriptor GetCodecByName(const char* strCodecName)
  {
    CodecDescriptor retVal;
    if (!strcmp(strCodecName, "MPEG2_VID"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("MPEG2VIDEO"), strCodecName); /* Video MPEG2 */
    else if (!strcmp(strCodecName, "H264_VID"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("H264"), strCodecName);       /* Video H264 */
	else if (!strcmp(strCodecName, "AC3_AUD"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("AC3"), strCodecName);        /* Áudio AC3 */
  	else if (!strcmp(strCodecName, "HEAA3_AUD"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("EAC3"), strCodecName);       /* Áudio EAC3 - Não confirmado mas pode ser DTS. */
  	else if (!strcmp(strCodecName, "MPEG1_AUD") || !strcmp(strCodecName, "MPEG2_AUD"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("MP2"), strCodecName);        /* Áudio MPEG1/2 */
  	else if (!strcmp(strCodecName, "AAC_AUD"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("AAC"), strCodecName);        /* Áudio AAC */
    else if (!strcmp(strCodecName, "TEXTSUB"))
      retVal = CodecDescriptor(CODEC->GetCodecByName("TEXT"), strCodecName);       /* Para Legenda mais não está sendo utilizado ainda. */
    else
      retVal = CodecDescriptor(CODEC->GetCodecByName(strCodecName), strCodecName);

    return retVal;
  }

private:
  xbmc_codec_t m_codec;
  std::string  m_strName;
};

#endif	/* XBMC_CODEC_DESCRIPTOR_HPP */