#pragma once
/*
 *      Copyright (C) 2011 Pulse-Eight
 *      http://www.pulse-eight.com/
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */

#include <pthread.h>
#include <vector>
#include "p8-platform/util/StdString.h"
#include "kodi/xbmc_stream_utils.hpp"
#include "client.h"

struct REEpgEntry
{
  int         iBroadcastId;
  std::string strTitle;
  int         iChannelId;
  time_t      startTime;
  time_t      endTime;
  std::string strPlotOutline;
  std::string strPlot;
  std::string strIconPath;
  int         iGenreType;
  int         iGenreSubType;
//  time_t      firstAired;
//  int         iParentalRating;
//  int         iStarRating;
//  bool        bNotify;
//  int         iSeriesNumber;
//  int         iEpisodeNumber;
//  int         iEpisodePartNumber;
//  std::string strEpisodeName;
};

struct REStreamInfo
{
  int         val;
  std::string type;
  std::string format;
  std::string lang;
};

struct REChannel
{
  bool                    bRadio;
  int                     iCaid;
  int                     iTuner;
  int                     iUniqueId;
  int                     iChannelNumber;
  int                     iSubChannelNumber;
  int                     iEncryptionSystem;
  std::string             strChannelName;
  std::string             strIconPath;
  std::string             strStreamURL;
  std::string             strServiceReference;
  std::vector<REStreamInfo> streams;
  std::vector<REEpgEntry> epg;
};

struct RERecording
{
  int         iDuration;
  int         iGenreType;
  int         iGenreSubType;
  std::string strChannelName;
  std::string strPlotOutline;
  std::string strPlot;
  std::string strRecordingId;
  std::string strStreamURL;
  std::string strTitle;
  std::string strDirectory;
  time_t      recordingTime;
};

struct RETimer
{
  int             iChannelId;
  time_t          startTime;
  time_t          endTime;
  PVR_TIMER_STATE state;
  std::string     strTitle;
  std::string     strSummary;
};

struct REChannelGroup
{
  bool             bRadio;
  int              iGroupId;
  std::string      strGroupName;
  int              iPosition;
  std::vector<int> members;
};

class RE
{
public:
  RE(void);
  virtual ~RE(void);
  
 /**
  * Singleton getter for the instance
  */
  static RE& GetInstance()
  {
    static RE re;
    return re;
  }

  virtual int GetChannelsAmount(void);
  virtual PVR_ERROR GetChannels(ADDON_HANDLE handle, bool bRadio);
  virtual bool GetChannel(const PVR_CHANNEL &channel, REChannel &myChannel);
  virtual PVR_ERROR GetStreamProperties(PVR_STREAM_PROPERTIES *props, REChannel channel);

  virtual int GetChannelGroupsAmount(void);
  virtual PVR_ERROR GetChannelGroups(ADDON_HANDLE handle, bool bRadio);
  virtual PVR_ERROR GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP &group);

  virtual PVR_ERROR GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL &channel, time_t iStart, time_t iEnd);

  virtual int GetRecordingsAmount(bool bDeleted);
  virtual PVR_ERROR GetRecordings(ADDON_HANDLE handle, bool bDeleted);

  virtual int GetTimersAmount(void);
  virtual PVR_ERROR GetTimers(ADDON_HANDLE handle);

  virtual std::string GetSettingsFile() const;
  
  virtual bool OpenLiveStream(REChannel channel);
  virtual void CloseLiveStream(REChannel channel);
  
  virtual DemuxPacket* DemuxRead(REChannel channel);
  
protected:
  virtual bool LoadDemoData(void);

private:
  virtual bool SendDVBCMD(std::string cmd);
  virtual void ParseXMLChannelToStream(std::string dados);
  static void *DVBServer(void *);
  static void *CDVBServer(void *socket_desc);
  
  std::vector<REChannelGroup>      m_groups;
  std::vector<REChannel>           m_channels;
  std::vector<RERecording>         m_recordings;
  std::vector<RERecording>         m_recordingsDeleted;
  std::vector<RETimer>             m_timers;
  time_t                           m_iEpgStart;
  CStdString                       m_strDefaultIcon;
  CStdString                       m_strDefaultMovie;
  pthread_t 			  		   m_lDVBServer;
  ADDON::XbmcStreamProperties      m_streams;
  std::map<int,int>                m_streamStat;
};
